#include "Cube.hpp"

int main()
{
  Cube<5> cube;

  if (cube.find())
    std::cout << cube;
  else
    std::cout << "Impossible to resolve" << std::endl;

  return 0;
}
