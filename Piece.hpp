#ifndef PIECE_HPP_
# define PIECE_HPP_

# include <iostream>
# include <vector>

class Piece
{
private:
  static std::vector<std::vector<std::vector<std::vector<bool>>>> _states;
  static bool _isStatesInit;

public:
  Piece();
  Piece(const Piece & o);
  Piece & operator=(const Piece & o);
  virtual ~Piece();

  const std::vector<std::vector<std::vector<bool>>> & operator[](unsigned int state) const;
  unsigned int size() const;

  unsigned int getLastState() const;

protected:
  mutable unsigned int _lastState;

private:
  void _initStates() const;
};

std::ostream & operator<<(std::ostream & os, const Piece & piece);
std::ostream & operator<<(std::ostream & os, const std::vector<std::vector<std::vector<bool>>> & piece);

#endif /* !PIECE_HPP_ */
