#include "Piece.hpp"

std::vector<std::vector<std::vector<std::vector<bool>>>> Piece::_states;
bool Piece::_isStatesInit = false;


Piece::Piece()
  : _lastState(0)
{
  if (!_isStatesInit)
    _initStates();
}

Piece::Piece(const Piece & o)
  : _lastState(o._lastState)
{

}

Piece & Piece::operator=(const Piece & o)
{
  if (this == &o)
    return *this;
  _lastState = o._lastState;
  return *this;
}

Piece::~Piece()
{

}

const std::vector<std::vector<std::vector<bool>>> & Piece::operator[](unsigned int state) const
{
  _lastState = state;
  return _states[state];
}

unsigned int Piece::size() const
{
  return _states.size();
}

unsigned int Piece::getLastState() const
{
  return _lastState;
}

void Piece::_initStates() const
{
  _states.resize(24);
  for (unsigned int i = 0; i < 24; ++i)
  {
    if (i < 8)  // 0 <= i < 8
    {
      _states[i].resize(4);
      for (unsigned int x = 0; x < 4; ++x)
      {
        if (i < 4)
        {
          _states[i][x].resize(2);
          for (unsigned int y = 0; y < 2; ++y)
            _states[i][x][y].resize(1, true);
        }
        else
        {
          _states[i][x].resize(1);
          _states[i][x][0].resize(2, true);
        }
      }
    }
    else if (i < 16) // 8 <= i < 16
    {
      _states[i].resize(2);
      for (unsigned int x = 0; x < 2; ++x)
      {
        if (i < 12)
        {
          _states[i][x].resize(4);
          for (unsigned int y = 0; y < 4; ++y)
            _states[i][x][y].resize(1, true);
        }
        else
        {
          _states[i][x].resize(1);
          _states[i][x][0].resize(4, true);
        }
      }
    }
    else // 16 <= i < 24
    {
      _states[i].resize(1);
      if (i < 20)
      {
        _states[i][0].resize(4);
        for (unsigned int y = 0; y < 4; ++y)
          _states[i][0][y].resize(2, true);
      }
      else
      {
        _states[i][0].resize(2);
        for (unsigned int y = 0; y < 2; ++y)
          _states[i][0][y].resize(4, true);
      }
    }
  }

  _states[0][0][0][0] = false;
  _states[0][1][0][0] = false;
  _states[0][3][1][0] = false;
  _states[1][0][1][0] = false;
  _states[1][2][0][0] = false;
  _states[1][3][0][0] = false;
  _states[2][0][0][0] = false;
  _states[2][2][1][0] = false;
  _states[2][3][1][0] = false;
  _states[3][0][1][0] = false;
  _states[3][1][1][0] = false;
  _states[3][3][0][0] = false;

  _states[4][0][0][0] = false;
  _states[4][1][0][0] = false;
  _states[4][3][0][1] = false;
  _states[5][0][0][1] = false;
  _states[5][2][0][0] = false;
  _states[5][3][0][0] = false;
  _states[6][0][0][0] = false;
  _states[6][2][0][1] = false;
  _states[6][3][0][1] = false;
  _states[7][0][0][1] = false;
  _states[7][1][0][1] = false;
  _states[7][3][0][0] = false;

  _states[8][0][0][0] = false;
  _states[8][0][1][0] = false;
  _states[8][1][3][0] = false;
  _states[9][0][2][0] = false;
  _states[9][0][3][0] = false;
  _states[9][1][0][0] = false;
  _states[10][0][0][0] = false;
  _states[10][1][2][0] = false;
  _states[10][1][3][0] = false;
  _states[11][0][3][0] = false;
  _states[11][1][0][0] = false;
  _states[11][1][1][0] = false;

  _states[12][0][0][0] = false;
  _states[12][0][0][1] = false;
  _states[12][1][0][3] = false;
  _states[13][0][0][2] = false;
  _states[13][0][0][3] = false;
  _states[13][1][0][0] = false;
  _states[14][0][0][0] = false;
  _states[14][1][0][2] = false;
  _states[14][1][0][3] = false;
  _states[15][0][0][3] = false;
  _states[15][1][0][0] = false;
  _states[15][1][0][1] = false;

  _states[16][0][0][0] = false;
  _states[16][0][1][0] = false;
  _states[16][0][3][1] = false;
  _states[17][0][0][1] = false;
  _states[17][0][2][0] = false;
  _states[17][0][3][0] = false;
  _states[18][0][0][0] = false;
  _states[18][0][2][1] = false;
  _states[18][0][3][1] = false;
  _states[19][0][0][1] = false;
  _states[19][0][1][1] = false;
  _states[19][0][3][0] = false;

  _states[20][0][0][0] = false;
  _states[20][0][0][1] = false;
  _states[20][0][1][3] = false;
  _states[21][0][0][2] = false;
  _states[21][0][0][3] = false;
  _states[21][0][1][0] = false;
  _states[22][0][0][0] = false;
  _states[22][0][1][2] = false;
  _states[22][0][1][3] = false;
  _states[23][0][0][3] = false;
  _states[23][0][1][0] = false;
  _states[23][0][1][1] = false;

  _isStatesInit = true;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
  for (unsigned int i = 0; i < piece.size(); ++i)
  {
    if (i)
      os << std::endl;
    os << "state[" << i << "]" << std::endl << piece[i] << std::endl;
  }
  return os;
}

std::ostream & operator<<(std::ostream & os, const std::vector<std::vector<std::vector<bool>>> & piece)
{
  for (unsigned int z = 0; z < piece[0][0].size(); ++z)
  {
    if (z)
      os << std::endl;
    for (unsigned int y = 0; y < piece[0].size(); ++y)
    {
      for (unsigned int x = 0; x < piece.size(); ++x)
        os << (piece[x][y][z] ? 'X' : 'O');
      os << std::endl;
    }
  }
  return os;
}
