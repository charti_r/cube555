#ifndef CUBE_HPP_
# define CUBE_HPP_

# include <iostream>
# include <iomanip>
# include <vector>

# include "Piece.hpp"

template <unsigned int S>
class Cube
{
public:
  Cube()
    : _arr(S, std::vector<std::vector<unsigned int>>(S, std::vector<unsigned int>(S, 0)))
    , _pieces(S*S)
  {

  }

  Cube(const Cube & o)
    : _arr(o._arr)
    , _pieces(o._pieces)
  {

  }

  Cube & operator=(const Cube & o)
  {
    if (this == &o)
      return *this;
    _arr = o._arr;
    _pieces = o._pieces;
    return *this;
  }

  virtual ~Cube()
  {

  }

  bool find()
  {
    return _backTrack(0);
  }

  std::ostream & print(std::ostream & os = std::cout) const
  {
    for (unsigned int y = 0; y < S; ++y)
    {
      for (unsigned int z = 0; z < S; ++z)
      {
        for (unsigned int x = 0; x < S; ++x)
          os << (x ? " " : "") << std::setfill('0') << std::setw(2) << _arr[x][y][z];
        os << "\t";
      }
      os << std::endl;
    }
    return os;
  }

protected:
  void _getPos(unsigned int pos, unsigned int & x, unsigned int & y, unsigned int & z)
  {
    x = pos % S;
    y = (pos / S) % S;
    z = pos / (S*S);
  }

private:
  void _debugPrint(unsigned int piece)
  {
    static unsigned int maxPiece = 0;

    if (piece > maxPiece)
      {
	maxPiece = piece;
	std::cout << "Fail to place piece n." << (piece + 1) << std::endl;
	print();
	std::cout << std::endl;
      }
  }

  bool _backTrack(const unsigned int piece)
  {
    unsigned int nbEmptyCase;

    if (piece == _pieces.size())
      return true;

    for (unsigned int pos = 0; pos < _maxPos; ++pos)
      for (unsigned int i = 0; i < _pieces[piece].size(); ++i)
        if (_isValid(_pieces[piece][i], pos))
	  {
          _savePiece(_pieces[piece][i], pos, piece);
	  nbEmptyCase = _countEmptyCase(pos);
	  if (!nbEmptyCase && _backTrack(piece + 1))
	    return true;
          _removePiece(_pieces[piece][i], pos);
	  if (nbEmptyCase >= 2)
	    return false;
        }
    return false;
  }

  bool _isValid(const std::vector<std::vector<std::vector<bool>>> & piece, unsigned int pos)
  {
    unsigned int ox, oy, oz;

    _getPos(pos, ox, oy, oz);
    if (ox + piece.size() > S || oy + piece[0].size() > S || oz + piece[0][0].size() > S)
      return false;
    for (unsigned int x = 0; x < piece.size(); ++x)
      for (unsigned int y = 0; y < piece[x].size(); ++y)
        for (unsigned int z = 0; z < piece[x][y].size(); ++z)
          if (piece[x][y][z] && _arr[ox + x][oy + y][oz + z])
            return false;
    return true;
  }

  unsigned int _countEmptyCase(unsigned int pos)
  {
    unsigned int count;

    count = 0;
    for (unsigned int i = 0; i <= pos; ++i)
      if (!_arr[i % S][(i / S) % S][i / (S*S)])
	++count;
    return count;
  }

  void _savePiece(const std::vector<std::vector<std::vector<bool>>> & piece, unsigned int pos, unsigned int number)
  {
    for (unsigned int x = 0; x < piece.size(); ++x)
      for (unsigned int y = 0; y < piece[x].size(); ++y)
        for (unsigned int z = 0; z < piece[x][y].size(); ++z)
          if (piece[x][y][z])
            _arr[(pos % S) + x][((pos / S) % S) + y][(pos / (S*S)) + z] = number + 1;
  }

  void _removePiece(const std::vector<std::vector<std::vector<bool>>> & piece, unsigned int pos)
  {
    for (unsigned int x = 0; x < piece.size(); ++x)
      for (unsigned int y = 0; y < piece[x].size(); ++y)
        for (unsigned int z = 0; z < piece[x][y].size(); ++z)
          if (piece[x][y][z])
            _arr[(pos % S) + x][((pos / S) % S) + y][(pos / (S*S)) + z] = 0;
  }

protected:
  std::vector<std::vector<std::vector<unsigned int>>>  _arr;
  const unsigned int _maxPos = S*S*S;
  std::vector<Piece> _pieces;
};

template <unsigned int S>
std::ostream & operator<<(std::ostream & os, const Cube<S> & cube)
{
  return cube.print(os);
}

#endif /* !CUBE_HPP_ */
